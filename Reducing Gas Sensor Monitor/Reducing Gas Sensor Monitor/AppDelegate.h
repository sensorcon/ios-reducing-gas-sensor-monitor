//
//  AppDelegate.h
//  Reducing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 12/12/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SensordroneiOSLibrary.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate, DroneEventDelegate>

@property (strong, nonatomic) UIWindow *window;
@property Drone *myDrone;

-(void)showConnectionLostDialog;
@end
