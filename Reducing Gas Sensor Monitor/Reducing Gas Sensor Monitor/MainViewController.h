//
//  MainViewController.h
//  Reducing Gas Sensor Monitor
//
//  Created by Mark Rudolph on 12/12/13.
//  Copyright (c) 2013 Sensorcon, Inc. All rights reserved.
//

#import "BaseViewController.h"
#import "SensordroneiOSLibrary.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

float threshold_0[3] = {1,      1,      1};
float threshold_1[3] = {0.7,    0.85,   0.925};
float threshold_2[3] = {0.6,    0.8,    0.9};
float threshold_3[3] = {0.5,    0.75,   0.875};
float threshold_4[3] = {0.4,    0.7,    0.85};
float threshold_5[3] = {0.3,    0.65,   0.825};
float threshold_6[3] = {0.2,    0.6,    0.8};
float threshold_7[3] = {0.1,    0.55,   0.775};
float threshold_8[3] = {0.09,   0.545,  0.7725};

@interface MainViewController : BaseViewController <DroneEventDelegate, AVAudioPlayerDelegate>

@property NSMutableArray *baselineArray;
@property float baseline;
@property int counter;
@property NSTimer *timer;
@property int selectedSensitivity;

// Data
@property (weak, nonatomic) IBOutlet UILabel *dataDisplay;


// Buttons
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_Low;
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_Med;
@property (weak, nonatomic) IBOutlet UIButton *sensBtn_High;
@property (weak, nonatomic) IBOutlet UIButton *blBtn_Reset;
@property (weak, nonatomic) IBOutlet UIButton *powerBtn;

// LEDs
@property (weak, nonatomic) IBOutlet UIImageView *led_1;
@property (weak, nonatomic) IBOutlet UIImageView *led_2;
@property (weak, nonatomic) IBOutlet UIImageView *led_3;
@property (weak, nonatomic) IBOutlet UIImageView *led_4;
@property (weak, nonatomic) IBOutlet UIImageView *led_5;
@property (weak, nonatomic) IBOutlet UIImageView *led_6;
@property (weak, nonatomic) IBOutlet UIImageView *led_7;
@property (weak, nonatomic) IBOutlet UIImageView *led_8;
@property (weak, nonatomic) IBOutlet UIImageView *led_9;

// Audio
@property (nonatomic, retain) AVAudioPlayer *geiger;


- (IBAction)sensitivitySelected:(id)sender;
- (IBAction)baselineReset:(id)sender;
- (IBAction)powerToggle:(id)sender;

-(void)calculateBaseline;
-(void)setSensitivity:(int)level;
-(void)setLEDs:(int)nLit;
-(void)playGeigerTicks:(int)nTicks;

@end
